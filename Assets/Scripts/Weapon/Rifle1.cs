﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle1 : Shooting {

    public GameObject HitParticles;
    public ParticleSystem Muzzle;

    RaycastHit Hit;

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && bullet > 0)
        {


            Muzzle.Play(true);

            OnStartShoot();
            if (Physics.Raycast(ShootPoint.position, ShootPoint.forward, out Hit, 10000))
            {

                GameObject hitEffect = (GameObject)Instantiate(HitParticles, Hit.point, Quaternion.identity);
                Destroy(hitEffect, 2);

                Humans Humans;
                if ((Humans = Hit.collider.GetComponent<Humans>()) != null)
                {
                    Humans.Damage(damage);
                    
                }

            }
        }
    }
}
