﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {


    public Transform ShootPoint;

    public int bullet = 20;

    public int damage = 50;


    void Start()
    {
        if (UIController.OnShoot != null)
        {
            UIController.OnShoot(bullet);
        }
    }

    public void OnStartShoot()
    {
        bullet--;

        if (UIController.OnShoot != null)
        {
            UIController.OnShoot(bullet);
        }
    }
}
