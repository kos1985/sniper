﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {

    private void OnEnable()
    {
        gameObject.GetComponent<AudioSource>().Play();
           
    }

    private void OnDisable()
    {
        gameObject.GetComponent<AudioSource>().Stop();

    }
}
