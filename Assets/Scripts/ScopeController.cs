﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScopeController : MonoBehaviour {

    float max_fieldOfView;
    float min_fieldOfView;

    public Image ScopeImage;

    void Awake () {

        max_fieldOfView = 60f;
        min_fieldOfView = 15f;

        ScopeImage.enabled = false;
        Camera.main.fieldOfView = max_fieldOfView;
	}
	
	
	void Update () {

        if (Input.GetMouseButton(1))
        {
            Camera.main.fieldOfView = min_fieldOfView;
            ScopeImage.enabled = true;
        }
        else {
            Camera.main.fieldOfView = max_fieldOfView;
            ScopeImage.enabled = false;
        }
		
	}
}
