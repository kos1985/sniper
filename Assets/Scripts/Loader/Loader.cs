﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{
    public Image LoadingImage;
    public Text LoadingText;

    float loadTime = 1;
    float curTime = 0;

    
    void Start()
    {
        StartCoroutine(DoLoad());

    }

    IEnumerator DoLoad()
    {
        System.GC.Collect();

        do
        {
            curTime += Time.deltaTime;
            var time = curTime / loadTime;
            LoadingImage.fillAmount = time;
            if (time > 1)
            {
                time = 1f;
            }
            LoadingText.text = string.Format("{0:0}%", time * 100);
            yield return new WaitForEndOfFrame();
        } while (curTime < loadTime);

        if (string.IsNullOrEmpty(CustomLevelLoader.NextScene))
        {
            CustomLevelLoader.NextScene = "menu"; //если запуситил первый раз
                                                  //то переменная будет пустая и мы загружаем menu
        }

        SceneManager.LoadScene(CustomLevelLoader.NextScene);


    }
}
