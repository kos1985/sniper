﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Sensivity;
    // Минимальное и макимальное значение вращения по горизонтали
    int minX = -60;
    int maxX = 60;

    // Минимальное и макимальное значение вращения по вертикали
    int minY = -30;
    int maxY = 30;

    float mouseX;
    float mouseY;

    Vector3 PlayerRotation;

    //Центрироание курсора
    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Start()
    {
        Sensivity = 0.1f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = false;

    }


    void Update()
    {
        mouseX = Mathf.Clamp((Input.mousePosition.x - Screen.width / 2) * Sensivity, minX, maxX);
        mouseY = Mathf.Clamp((Input.mousePosition.y - Screen.height / 2) * (-1) * Sensivity, minY, maxY);

        PlayerRotation.Set(mouseY, mouseX, 0);

        transform.eulerAngles = PlayerRotation;



    }
}
