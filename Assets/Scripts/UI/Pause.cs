﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

    public GameObject PausePanel;
    public PlayerController PlayerController;
    public ScopeController ScopeController;


   

    public bool isPaused;


	void Start () {

        Time.timeScale = 1f;
        isPaused = false;
        PausePanel.SetActive(false);


    }
	
	
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape)) {

            
            isPaused = !isPaused;
            
        }

        if (isPaused)
        {
            ScopeController.enabled = false;
            PlayerController.enabled = false;
            Cursor.visible = true;
            Time.timeScale = 0;
            PausePanel.SetActive(true);

        }

        else
        {
            ScopeController.enabled = true;
            PlayerController.enabled = true;
            Cursor.visible = false;
            Time.timeScale = 1;
            PausePanel.SetActive(false);

        }


    }


}
