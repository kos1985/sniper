﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundController : MonoBehaviour {

    
    public Slider SoundSlider;

	void Start () {

        
        SoundSlider.value = CustomSettings.Volume;
        
    }
	
	
	void Update () {

        CustomSettings.Volume = SoundSlider.value;
        AudioListener.volume = CustomSettings.Volume;
		
	}
}
