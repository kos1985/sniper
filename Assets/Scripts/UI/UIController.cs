﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class UIController : MonoBehaviour {

    public GameObject PausePanel;
    public PlayerController PlayerController;

    public string CurrentLevel;


    [SerializeField] Text BulletText;

    public static Action<int> OnShoot;


    void Awake()
    {
        OnShoot += BulletRefresh;
    }

    void OnDestroy()
    {
        OnShoot -= BulletRefresh;
    }

    void BulletRefresh(int bullet)
    {

        BulletText.text = bullet.ToString();

    }


    public void NewGame()
    {
        CustomLevelLoader.LoadLevel("Level1");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ContinueGame(GameObject obj)
    {
        obj.GetComponent<Pause>().isPaused = false;

    }

    public void RestartLevel(GameObject obj)
    {
        
        CustomLevelLoader.LoadLevel("Level1");
        obj.GetComponent<Pause>().isPaused = false;

        //SceneManager.LoadScene("Level1");
    }
}
